$(".video-open").click( function() {
  $(".video").addClass("video-visible");
  document.getElementById('video-iframe').contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
});

$(".video-close").click( function() {
  $(".video").removeClass("video-visible");
  document.getElementById('video-iframe').contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
});
